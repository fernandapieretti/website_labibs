/* JS Document
*
* @autor Fernanda Pieretti
* @data jul2015
* @ref scripts.js
*
*/

$(window).load(function(){
	//Menu
	$("#menuDropDown").click(function() {
		if ($("#nav ul").hasClass("ativo")) {
			$("#nav ul").removeClass("ativo");
			$("#menuDropDown").removeClass("fechar");
		} else {
			$("#nav ul").addClass("ativo");
			$("#menuDropDown").addClass("fechar");
		}
	});	
	
	//Slider
    $(".flexslider").flexslider({
        animation: "slide",
        directionNav: true,
        controlNav: false,
        start: function (slider) {
            $(".flexslider").removeClass('loading');
        }
    });
});